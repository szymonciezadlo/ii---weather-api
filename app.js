const express=require('express');
const app=express();
const morgan = require('morgan');
const mongoose=require('mongoose');
const bodyParser=require('body-parser');

const weatherRoutes=require('./api/weather/index');
const citiesRoutes=require('./api/cities/index');
const usersRoutes=require('./api/users/index');

mongoose.connect('mongodb+srv://saimono003:'+process.env.MONGODB_PASSWORD+'@cluster0-jr7gs.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

//Morgan logi na serwerze
app.use(morgan('dev'));
//Body-parser html->zmienne
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//ROUTINGI
app.use('/weather',weatherRoutes);
app.use('/cities',citiesRoutes);
app.use('/users',usersRoutes);

/*
//CORS
app.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, Authorization");
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods','PUT,PATCH,POST,DELETE,GET');
        return res.status(200).json({});
    }
    next();
});
*/

app.use((req,res,next)=>{
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error,req,res,next)=>{
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});
module.exports=app;