var express = require('express');
var request=require('request');
//var mongoose=require('mongoose');

var app=express();
app.set('view engine','ejs');

var city="Warsaw";
var url='http://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&appid=404e9b08bf18af53a14aa3d57824469c';

/*
app.use((req,res,next)=> {
    res.status(200).json({message:'It works!'});
});
*/

//pierwszy get pobiera pogode w request
app.get('/',function (req,res) {
    request(url,function (error,response,body) {
       weather_json=JSON.parse(body);
        console.log(weather_json);
        var weather={
            city : city,
            temperature : weather_json.main.temp,
            description: weather_json.weather[0].description,
            icon: weather_json.weather[0].icon
        };
        var weather_data={weather:weather};

        res.render('weather',weather_data);
    });


});

module.exports=app;