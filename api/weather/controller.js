const request=require('request');

const show = ({params},res,next) => {
    const city=params.id;
    var url='http://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&appid=404e9b08bf18af53a14aa3d57824469c';
    request(url,function (error,response,body) {
        weather_json=JSON.parse(body);
        var weather={
            city : city,
            temperature : weather_json.main.temp,
            description: weather_json.weather[0].description,
            icon: weather_json.weather[0].icon
        };
        var weather_data={weather:weather};
        console.log(weather_data);
        res.status(200).json(weather_json);
    });

};

module.exports={
    show
};