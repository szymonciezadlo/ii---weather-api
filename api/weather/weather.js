const express = require('express');
const router=express.Router();
const request=require('request');

router.get('/', (req,res,next) => {
    res.status(200).json({
        message: 'Handling GET'
    });
});
router.get('/:cityName', (req,res,next) => {
    const city=req.params.cityName;
    var url='http://api.openweathermap.org/data/2.5/weather?q='+city+'&units=metric&appid=404e9b08bf18af53a14aa3d57824469c';
    request(url,function (error,response,body) {
        weather_json=JSON.parse(body);
        var weather={
            city : city,
            temperature : weather_json.main.temp,
            description: weather_json.weather[0].description,
            icon: weather_json.weather[0].icon
        };
        var weather_data={weather:weather};
        console.log(weather_data)

        res.status(200).json(weather_json);
    });

});

module.exports=router;