const { Router } = require('express');
const { show } = require('./controller');

const router = Router();
router.get('/:id',
    show);

module.exports = router;
