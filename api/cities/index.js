const { Router } = require('express');
const { create, index, show, update, destroy,weather,forecast,historyForecast } = require('./controller');
const checkAuth=require('../middleware/check-auth')


const router = Router();
router.get('/', index);
router.post('/:cityName',checkAuth, create);
router.put('/:id',checkAuth, update);
router.get('/:id',show);
router.get('/:id/weather', weather);
router.get('/:id/forecast', forecast);
router.get('/:id/historyForecast', historyForecast);

/*


router.post('/',
    create);



router.delete('/:id',
    destroy);
*/

module.exports = router;
