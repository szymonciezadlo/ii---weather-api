const {City}=require('./model');
//const {getW}=require('../weather/getWeather')
const mongoose=require('mongoose');
const Joi = require('joi');
const request = require('request');
var fetch=require('node-fetch');

const index = async ({query}, res, next) => {
    // Aby kod byl czystszy nalezaloby przeniesc walidacje do middlewaru
    const qValidtor = Joi.object({
        limit: Joi.number().integer().max(30).default(1000),
        skip: Joi.number().integer().default(0),
        name: Joi.string()
    }).validate(query);
    if(qValidtor.error) return res.status(400).end();
    const {skip, limit, name} = qValidtor.value;
    const mongoQuery = {};
    if(name) mongoQuery.name = name;
    // Chcemy dwie informacje: Ile jest wszystkich ksiazek pasujacych do zapytania + odpowiedz na nasze zapytanie wliczajac przyjete limity.
    let results = [];
    try {
        results = await Promise.all( [
            City.find(mongoQuery).skip(skip).limit(limit),
            City.countDocuments(mongoQuery)
        ]);
    } catch (e) {
        return next(e)
    }
    return res
        .status(200)
        .set('X-Total-Count', results[1])               // Ile jest wszystkich potencjalnych wynikow pasujacych do query
        .set('X-Result-Count', results[0].length)       // Ile zwrocono wynikow, opcjonalne, ale przydatne jesli klient nie zna limitu domyslnego
        .json(results[0]);                              // Zwrocone wyniki (uwzgledniajac limit)
};

const create = async ({params}, res, next) => {
    try {
        const cityName=params.cityName;
        const url='http://api.openweathermap.org/data/2.5/weather?q='+cityName+'&units=metric&appid=404e9b08bf18af53a14aa3d57824469c';
        const fetch_res=await fetch(url);
        if(!fetch_res.ok){
            console.log(fetch_res.status)
            return res.status(404).json("City not found 404");
        }
        const weather_json= await fetch_res.json();
        const city={
            cityName: weather_json.name,
            localization: {
                X: weather_json.coord.lon,
                Y: weather_json.coord.lat
            }
        };
        await City.create(city);
        return res.status(201).json(city);
    } catch (e) {
        return next(e);
    }
};

const update=async ({body, params}, res, next) => {
    const id = params.id;
    const {cityName} = body;
    try {
        const city = await City.findById(id);
        if (city) {
            var url='http://api.openweathermap.org/data/2.5/weather?q='+cityName+'&units=metric&appid=404e9b08bf18af53a14aa3d57824469c';
            request.get(url, function (error,response,body){
                weather_json=JSON.parse(body);
                var weather={
                    cityName : weather_json.name,
                    localization: {
                        X:weather_json.coord.lon,
                        Y:weather_json.coord.lat
                    }
                };
                city.cityName = weather.cityName;
                city.localization={
                    X: weather.localization.X,
                    Y: weather.localization.Y
                };
                city.save();
                return res.json(city);
            });
        } else {
            return res.status(404);
        }
    } catch (e) {
        next(e);
    }
};

const show = async ({params}, res, next) => {
    const id = params.id;       // ID jest stringiem (UUID)
    try {
        const city = await City.findById(id);
        if (city) return res.status(200).json(city);
        return res.status(404);
    } catch (e) {
        next(e);
    }
};

const weather=async ({params}, res, next) => {
    const id = params.id;           // ID jest stringiem (UUID)
    try {
        const city = await City.findById(id);
        var url='http://api.openweathermap.org/data/2.5/weather?q='+city.cityName+'&units=metric&appid=404e9b08bf18af53a14aa3d57824469c';
        request.get(url, function (error,response,body) {
            weather_json = JSON.parse(body);
            return res.status(200).json(weather_json);
            return res.status(404);
        });
    } catch (e) {
        next(e);
    }
};

const forecast=async ({params}, res, next) => {
    const id = params.id;
    try {
        const city = await City.findById(id);
        if (city) {
            const url='https://api.darksky.net/forecast/713061eaae50a4cb67962dc838bea665/'+city.localization.X+','+city.localization.Y+'?units=ca&exclude=hourly'
            const fetch_response=await fetch(url);
            const weather_json= await fetch_response.json();
            res.status(200).json(weather_json);
        }
    } catch (e) {
        next(e);
    }
};

const historyForecast=async ({params}, res, next) => {
    const id = params.id;
    try {
        const city = await City.findById(id);
        if (city) {
            const url='https://api.darksky.net/forecast/713061eaae50a4cb67962dc838bea665/'+city.localization.X+','+city.localization.Y+'?units=ca&exclude=hourly'
            const fetch_response=await fetch(url);
            const weather_json= await fetch_response.json();
            res.status(200).json(weather_json);
        }
    } catch (e) {
        next(e);
    }
};
module.exports={index,create,update,show,weather,forecast,historyForecast};