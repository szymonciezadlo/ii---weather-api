const Mongoose=require('mongoose');
const Schema=Mongoose.Schema;

const CitySchema = new Schema({
    cityName: {
        type: String,
        required: true
    },
    localization: {
        X:Number,
        Y:Number
    }
},{
    timestamps: true,
    toJSON: {
        transform: (obj,ret)=>{
            delete ret['__v']
        }
    }
});

//module.exports=mongoose.model("City",citySchema);
module.exports={
    CitySchema,
    City: Mongoose.model('City',CitySchema)
}