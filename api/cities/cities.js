const express=require('express');
const router=express.Router();

const City=require('./City');
const mongoose=require('mongoose');

router.get('/', (req,res,next) => {
    res.status(200).json({
        message: 'Handling GET'
    });
});

router.post('/', (req,res,next) => {
    const city= new City({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name
    });
    city.save().
        then(result => {console.log(result);})
            .catch(err=>console.log(err));
    res.status(200).json({
        message: 'Handling POST'
    });
});


module.exports=router;