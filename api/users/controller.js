const {User}=require('./model');
const mongoose = require('mongoose');
const bcrypt=require('bcrypt');
const jwt=require('jsonwebtoken')

const signup=async ({body}, res, next) => {
    if(body.password<6)return res.status(400).json({
        message:"Password too short"
    });
    User.find({ email: body.email })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.status(409).json({
                    message: "Mail exists"
                });
            } else {
                bcrypt.hash(body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        });
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: body.email,
                            password: hash
                        });
                        User
                            .create(user)
                            .then(result => {
                                console.log(result);
                                res.status(201).json({
                                    message: "User created"
                                });
                            })
                            .catch(err => {
                                console.log(err);
                                res.status(500).json({
                                    error: err
                                });
                            });
                    }
                });
            }
        });
};

const signin=async ({body}, res, next) => {
    User.find({email:body.email})
        .exec()
        .then(user => {
        if(user.length<1){
            return res.status(401).json({
                message: "Email or password is wrong"})
            }
        else{
            bcrypt.compare(body.password,user[0].password, (err,result)=>{
                if(err){
                    return res.status(401).json({
                        message: "Email or password is wrong"})
                }
                if(result){
                    const token =jwt.sign({
                        email: user[0].email,
                        userId: user[0]._id
                    },process.env.JWT_KEY,{
                        expiresIn: "6h"
                    });
                    return res.status(200).json({
                        message: "Auth success",
                        token: token
                    })
                }
                return res.status(401).json({
                    message: "Email or password is wrong"})
            })
        }


        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });

})}

module.exports={signup,signin};