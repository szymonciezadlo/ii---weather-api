const Mongoose=require('mongoose');
const Schema=Mongoose.Schema;

const UserSchema=new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: {
        type: String,
        required: true,
    }
}, {
    timestamps: true,
    toJSON: {
        transform: (obj, ret) => {
            delete ret['__v']
        }
    }
});
module.exports={
    UserSchema,
    User: Mongoose.model('User',UserSchema)
}